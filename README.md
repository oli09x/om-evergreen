# OM Evergreen
*OM Evergreen* is **the** multipurpose Typo3 template for your enterprise needs. Fully responsive, SEO-friendly (by following Google recommendations among others) and 100% GDPR compliant.

Check out the **PRO** version for a multitude of customization options to make your website fully unique and adapt it to your CI.  
The **PRO** version can also be individually adjusted and extended to your specific needs.

## Installation
It is recommended to install *OM Evergreen* via Composer:  

```bash
composer require olivermelle/om-evergreen
```

[Instructions on how to install TYPO3 via Composer](https://docs.typo3.org/m/typo3/tutorial-getting-started/11.5/en-us/Installation/Install.html)  
[Instructions on how to migrate TYPO3 to Composer](https://docs.typo3.org/m/typo3/guide-installation/11.5/en-us/MigrateToComposer/Requirements.html)

## Configuration
### Add OM Evergreen TypoScript
Go to the module `Web > Template` and choose your root page. It soould already contain a template record. If it doesn’t, create one by clicking the button `Create template for a new site`. Switch to the view `Info/Modify` and click on `Edit the whole template record`.

When you just created a now template record, delete the automatically created Setup (starting with `# Default PAGE object:`).

![Tab "Includes"](Documentation/Images/template-includes.png)  
Switch to tab `Includes` and add `TypoScript needed for OM Evergreen(om_evergreen)`. Make sure you include this template **after** all other included templates. Do not include `Fluid Content Elements CSS`.

The templates `Fluid Content Elements` and `XML Sitemap` don’t have to be included, they will be automatically included by OM Evergreen’s TypoScript.

### Set backend layout
Go to the module `Web > Page` and select your root page.

![Toolbar of the page](Documentation/Images/page-toolbar.png)  
Go to the page properties by clicking the button `Edit page properties` (Right button)

![Tab "Appearance"](Documentation/Images/page-appearance.png)  
Switch to tab `Appearance` and select `Default` for `Backend layout (this page only)` and `Backend layout (subpages of this page)`.

## License
*OM Evergreen* is licensed under the GNU General Public License 2.0 or later.

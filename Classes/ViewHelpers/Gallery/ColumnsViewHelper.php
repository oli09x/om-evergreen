<?php
namespace Olivermelle\OmEvergreen\ViewHelpers\Gallery;

use Closure;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Exception as ViewHelperException;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

/**
 * This class implements a ViewHelper to add responsive column lengths to an imagegallery.
 */
final class ColumnsViewHelper extends AbstractViewHelper
{
    use CompileWithRenderStatic;

    protected $escapeOutput = false;

    public function initializeArguments()
    {
        $this
            ->registerArgument('columnCount', 'integer', 'Column length', true)
            ->registerArgument('name', 'string', 'Name of resulting variable', false, 'cols');
    }

    public static function renderStatic(array $arguments, Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        $templateVariableContainer = $renderingContext->getVariableProvider();

        $arguments['columnCount'] = intval($arguments['columnCount']);
        if ($arguments['columnCount'] == 0) {
            throw new ViewHelperException('Argument "columnCount" for ViewHelper "gallery.columns" must be an integer above 0');
        }

        $colsMd = $arguments['columnCount'] / 2;
        if ($colsMd < 2 && $arguments['columnCount'] > 1) {
            $colsMd = 2;
        }
        $cols = [
            "full" => $arguments['columnCount'],
            "xs" => ceil($arguments['columnCount'] / 4),
            "sm" => ceil($arguments['columnCount'] / 3),
            "md" => ceil($colsMd),
        ];

        $templateVariableContainer->add($arguments['name'], $cols);

        return $renderChildrenClosure();
    }
}

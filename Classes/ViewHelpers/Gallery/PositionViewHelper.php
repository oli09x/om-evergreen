<?php
namespace Olivermelle\OmEvergreen\ViewHelpers\Gallery;

use Closure;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Exception as ViewHelperException;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

/**
 * This class implements a ViewHelper to determine the position of an element in an responsive imagegallery.
 */
final class PositionViewHelper extends AbstractViewHelper
{
    use CompileWithRenderStatic;

    protected $escapeOutput = false;

    public function initializeArguments()
    {
        $this
            ->registerArgument('row', 'integer', 'Current row position', true)
            ->registerArgument('col', 'integer', 'Current column position', true)
            ->registerArgument('columnCounts', 'array', 'An array with the column lengths as returned by the ViewHelper \'gallery.columns\'', true)
            ->registerArgument('name', 'string', 'Name of resulting variable', false, 'pos')
            ->registerArgument('hasFeatured', 'boolean', 'Is the first image a featured image', false, false);
    }

    public static function renderStatic(array $arguments, Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        $templateVariableContainer = $renderingContext->getVariableProvider();

        $arguments['row'] = intval($arguments['row']);
        $arguments['col'] = intval($arguments['col']);
        if ($arguments['row'] == 0) {
            throw new ViewHelperException('Argument "row" for ViewHelper "gallery.position" must be an integer above 0');
        }
        if ($arguments['col'] == 0) {
            throw new ViewHelperException('Argument "col" for ViewHelper "gallery.position" must be an integer above 0');
        }
        if (array_diff(['full', 'xs', 'sm', 'md'], array_keys($arguments['columnCounts']))) {
            throw new ViewHelperException('Argument "columnCounts" for ViewHelper "gallery.position" must be an array as returned by "gallery.columns"');
        }

        $elemCount = ($arguments['row'] - 1) * $arguments['columnCounts']['full'] + $arguments['col'];
        $addFeaturedRow = 0;
        $addColumnSpan = false;
        if ($arguments['hasFeatured'] && $elemCount === 1) {
            $addColumnSpan = true;
        }
        if ($arguments['hasFeatured'] && $elemCount > 1) {
            $elemCount -= 1;
            $addFeaturedRow = 1;
        }

        $posFull = $elemCount % $arguments['columnCounts']['full'];
        $posXs = $elemCount % $arguments['columnCounts']['xs'];
        $posSm = $elemCount % $arguments['columnCounts']['sm'];
        $posMd = $elemCount % $arguments['columnCounts']['md'];
        $pos = [
            "cycle" => $elemCount,
            "row" => [
                "full" => ceil($elemCount / $arguments['columnCounts']['full']) + $addFeaturedRow,
                "xs" => ceil($elemCount / $arguments['columnCounts']['xs']) + $addFeaturedRow,
                "sm" => ceil($elemCount / $arguments['columnCounts']['sm']) + $addFeaturedRow,
                "md" => ceil($elemCount / $arguments['columnCounts']['md']) + $addFeaturedRow,
            ],
            "col" => [
                "full" => ($posFull > 0 ? $posFull : $arguments['columnCounts']['full']) . ($addColumnSpan ? "/span " . $arguments['columnCounts']['full'] : ""),
                "xs" => ($posXs > 0 ? $posXs : $arguments['columnCounts']['xs']) . ($addColumnSpan ? "/span " . $arguments['columnCounts']['xs'] : ""),
                "sm" => ($posSm > 0 ? $posSm : $arguments['columnCounts']['sm']) . ($addColumnSpan ? "/span " . $arguments['columnCounts']['sm'] : ""),
                "md" => ($posMd > 0 ? $posMd : $arguments['columnCounts']['md']) . ($addColumnSpan ? "/span " . $arguments['columnCounts']['md'] : ""),
            ],
        ];

        $templateVariableContainer->add($arguments['name'], $pos);

        return $renderChildrenClosure();
    }
}

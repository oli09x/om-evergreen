<?php
namespace Olivermelle\OmEvergreen\ViewHelpers\Color;

use Closure;
use Olivermelle\OmEvergreen\StaticHelper\WcagContrast;
use TYPO3\CMS\Core\Resource\FileReference;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

/**
 * Select an appropiate text color for a background image according to WCAG 2.0.
 */
final class SelectTextToBgImgViewHelper extends AbstractViewHelper
{
    use CompileWithRenderStatic;

    public function initializeArguments()
    {
        $this
            ->registerArgument('backgroundImage', FileReference::class, 'Background image to check against', true);
    }

    public static function renderStatic(array $arguments, Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        /** @var FileReference */
        $bgImage = $arguments['backgroundImage'];
        $bgLuminances = WcagContrast::getImageLightness($bgImage);

        if (WcagContrast::evaluateColorContrast(WcagContrast::$defaultDarkColor, $bgLuminances['min']['lum'])["levelAALarge"]) {
            return ["color" => "#" . WcagContrast::$defaultDarkColor];
        }
        if (WcagContrast::evaluateColorContrast(WcagContrast::$defaultLightColor, $bgLuminances['max']['lum'])["levelAALarge"]) {
            return ["color" => "#" . WcagContrast::$defaultLightColor];
        }
        $luminanceDark = WcagContrast::evaluateColorContrast('000000', $bgLuminances['min']['lum']);
        if ($luminanceDark["levelAALarge"]) {
            return ["color" => "#000"];
        }
        $luminanceLight = WcagContrast::evaluateColorContrast('FFFFFF', $bgLuminances['max']['lum']);
        if ($luminanceLight["levelAALarge"]) {
            return ["color" => "#fff"];
        }

        if ($luminanceDark['ratio'] > $luminanceLight['ratio']) {
            $lightenBy = 0;
            $lightenColor = "";

            while ($lightenBy += 0.2) {
                $lightenColor = WcagContrast::lightenColor($bgLuminances['min']['rgb'], $lightenBy);
                $lightLum = WcagContrast::evaluateColorContrast('000000', $lightenColor);
                if ($lightLum["levelAALarge"]) break;
            }

            return [
                "color" => "#000",
                "bgChange" => 1 + $lightenBy
            ];
        } else {
            $darkenBy = 0;
            $darkenColor = "";

            while ($darkenBy += 0.2) {
                $darkenColor = WcagContrast::darkenColor($bgLuminances['max']['rgb'], $darkenBy);
                $darkLum = WcagContrast::evaluateColorContrast('FFFFFF', $darkenColor);
                if ($darkLum["levelAALarge"]) break;
            }

            return [
                "color" => "#FFF",
                "bgChange" => 1 - $darkenBy
            ];
        }
    }
}

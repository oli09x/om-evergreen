<?php
namespace Olivermelle\OmEvergreen\ViewHelpers\Color;

use Closure;
use Olivermelle\OmEvergreen\StaticHelper\CleanStrings;
use Olivermelle\OmEvergreen\StaticHelper\WcagContrast;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

/**
 * Lighten/darken the background color for hover
 */
final class HoverColorAutoViewHelper extends AbstractViewHelper
{
    use CompileWithRenderStatic;

    public function initializeArguments()
    {
        $this
            ->registerArgument('color', 'string', 'Color in RGB hex value (e.g. #000000)', true);
    }

    public static function renderStatic(array $arguments, Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        /** @var string */
        $bgColor = CleanStrings::cleanHexColor($arguments['color']);
        $direction = '';

        if (WcagContrast::evaluateColorContrast(WcagContrast::$defaultDarkColor, $bgColor)["levelAANormal"]) {
            $direction = 'lighten';
        }
        if (WcagContrast::evaluateColorContrast(WcagContrast::$defaultLightColor, $bgColor)["levelAANormal"]) {
            $direction = 'darken';
        }
        if (WcagContrast::evaluateColorContrast('000000', $bgColor)["levelAANormal"]) {
            $direction = 'lighten';
        }
        if (WcagContrast::evaluateColorContrast('FFFFFF', $bgColor)["levelAANormal"]) {
            $direction = 'darken';
        }

        if ($direction == 'lighten') {
            return "#" . WcagContrast::lightenColor($bgColor, 0.2);
        } else {
            return "#" . WcagContrast::darkenColor($bgColor, 0.2);
        }
    }
}

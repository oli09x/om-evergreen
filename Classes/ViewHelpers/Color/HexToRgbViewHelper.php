<?php
namespace Olivermelle\OmEvergreen\ViewHelpers\Color;

use Closure;
use Olivermelle\OmEvergreen\StaticHelper\CleanStrings;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

/**
 * Converts a hex color to RGB values for the CSS rgb() function
 */
final class HexToRgbViewHelper extends AbstractViewHelper
{
    use CompileWithRenderStatic;

    public function initializeArguments()
    {
        $this
            ->registerArgument('color', 'string', 'Color in RGB hex value (e.g. #000000)', true);
    }

    public static function renderStatic(array $arguments, Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        $hexColor = CleanStrings::cleanHexColor($arguments['color']);
        $hexParts = str_split($hexColor, 2);
        return hexdec($hexParts[0]) . ',' . hexdec($hexParts[1]) . ',' . hexdec($hexParts[2]);
    }
}

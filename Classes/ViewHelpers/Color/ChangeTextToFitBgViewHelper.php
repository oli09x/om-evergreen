<?php
namespace Olivermelle\OmEvergreen\ViewHelpers\Color;

use Closure;
use Olivermelle\OmEvergreen\StaticHelper\CleanStrings;
use Olivermelle\OmEvergreen\StaticHelper\WcagContrast;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

/**
 * Lighten/darken the text color until it complies with WCAG 2
 */
final class ChangeTextToFitBgViewHelper extends AbstractViewHelper
{
    use CompileWithRenderStatic;

    public function initializeArguments()
    {
        $this
            ->registerArgument('color', 'string', 'Color in RGB hex value (e.g. #000000)', true)
            ->registerArgument('bgColor', 'string', 'Background color to compare against', true);
    }

    public static function renderStatic(array $arguments, Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        $color = CleanStrings::cleanHexColor($arguments['color']);
        $bgColor = CleanStrings::cleanHexColor($arguments['bgColor']);

        $luminanceDark = WcagContrast::evaluateColorContrast('000000', $bgColor);
        $luminanceLight = WcagContrast::evaluateColorContrast('FFFFFF', $bgColor);

        if ($luminanceDark['ratio'] > $luminanceLight['ratio']) {
            while (!WcagContrast::evaluateColorContrast($color, $bgColor)["levelAANormal"]) {
                $colorNew = WcagContrast::darkenColor($color);
                if ($colorNew === $color) break;
                $color = $colorNew;
            }
        } else {
            while (!WcagContrast::evaluateColorContrast($color, $bgColor)["levelAANormal"]) {
                $colorNew = WcagContrast::lightenColor($color);
                if ($colorNew === $color) break;
                $color = $colorNew;
            }
        }

        return "#" . $color;
    }
}

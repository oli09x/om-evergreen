<?php
namespace Olivermelle\OmEvergreen\ViewHelpers\Color;

use Closure;
use Olivermelle\OmEvergreen\StaticHelper\CleanStrings;
use Olivermelle\OmEvergreen\StaticHelper\WcagContrast;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

/**
 * Lighten/darken the text color for hover
 */
final class HoverColorViewHelper extends AbstractViewHelper
{
    use CompileWithRenderStatic;

    public function initializeArguments()
    {
        $this
            ->registerArgument('color', 'string', 'Color in RGB hex value (e.g. #000000)', true)
            ->registerArgument('direction', 'string', '\'lighten\' or \'darken\'', false, 'darken');
    }

    public static function renderStatic(array $arguments, Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        $color = CleanStrings::cleanHexColor($arguments['color']);

        if ($arguments['direction'] == 'lighten') {
            return "#" . WcagContrast::lightenColor($color, 0.2);
        } else {
            return "#" . WcagContrast::darkenColor($color, 0.2);
        }
    }
}

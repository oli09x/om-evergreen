<?php
namespace Olivermelle\OmEvergreen\ViewHelpers\Color;

use Closure;
use Olivermelle\OmEvergreen\StaticHelper\WcagContrast;
use Olivermelle\OmEvergreen\StaticHelper\CleanStrings;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

/**
 * Select an appropiate text color for a background color according to WCAG 2.0.
 */
final class SelectTextToBgViewHelper extends AbstractViewHelper
{
    use CompileWithRenderStatic;

    public function initializeArguments()
    {
        $this
            ->registerArgument('backgroundColor', 'string', 'Background color to check against', true);
    }

    public static function renderStatic(array $arguments, Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        /** @var string */
        $bgColor = CleanStrings::cleanHexColor($arguments['backgroundColor']);

        if (WcagContrast::evaluateColorContrast(WcagContrast::$defaultDarkColor, $bgColor)["levelAANormal"]) {
            return "#" . WcagContrast::$defaultDarkColor;
        }
        if (WcagContrast::evaluateColorContrast(WcagContrast::$defaultLightColor, $bgColor)["levelAANormal"]) {
            return "#" . WcagContrast::$defaultLightColor;
        }
        if (WcagContrast::evaluateColorContrast('000000', $bgColor)["levelAANormal"]) {
            return "#000";
        }
        if (WcagContrast::evaluateColorContrast('FFFFFF', $bgColor)["levelAANormal"]) {
            return "#fff";
        }
    }
}

<?php
namespace Olivermelle\OmEvergreen\StaticHelper;

use Olivermelle\OmEvergreen\Cache\ImgLuminance;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Resource\FileReference;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * This class contains static methods to return an appropiate text color for a background color according to WCAG 2.0.
 * 
 * This class uses methods originally written by https://github.com/gdkraus
 * 
 * @see https://webaim.org/resources/contrastchecker/ Online contrast checker and more information
 * @see https://github.com/gdkraus/wcag2-color-contrast/ Functions from this repository are used in this class
 */
class WcagContrast
{
    /** @var string Default light text color without hash (#) */
    public static $defaultLightColor = "FBFBFE";

    /** @var string Default dark text color without hash (#) */
    public static $defaultDarkColor = "171A1D";

    /** @var ImgLuminance */
    private static $luminanceCacheObj;

    /**
     * Calculates the luminosity of an given RGB.
     * 
     * @see https://www.w3.org/TR/WCAG20/#relativeluminancedef
     * 
     * @param string $color Color in the format RRGGBB without hash (#)
     * 
     * @return float Relative luminance of the color
     */
    private static function calculateLuminosity($color)
    {
        $r = hexdec(substr($color, 0, 2)) / 255; // red value
        $g = hexdec(substr($color, 2, 2)) / 255; // green value
        $b = hexdec(substr($color, 4, 2)) / 255; // blue value
        if ($r <= 0.03928) {
            $r = $r / 12.92;
        } else {
            $r = pow((($r + 0.055) / 1.055), 2.4);
        }
    
        if ($g <= 0.03928) {
            $g = $g / 12.92;
        } else {
            $g = pow((($g + 0.055) / 1.055), 2.4);
        }
    
        if ($b <= 0.03928) {
            $b = $b / 12.92;
        } else {
            $b = pow((($b + 0.055) / 1.055), 2.4);
        }
    
        $luminosity = 0.2126 * $r + 0.7152 * $g + 0.0722 * $b;
        return $luminosity;
    }

    /**
     * Calculates the luminosity ratio of two colors.
     * 
     * @see https://www.w3.org/TR/WCAG20/#contrast-ratiodef
     * 
     * @param string|float $color1 First color in the format RRGGBB without hash (#) or luminance as float
     * @param string|float $color2 Second color in the format RRGGBB without hash (#) or luminance as float
     * 
     * @return float Contrast ratio
     */
    private static function calculateLuminosityRatio($color1, $color2)
    {
        $l1 = is_float($color1) ? $color1 : self::calculateLuminosity($color1);
        $l2 = is_float($color2) ? $color2 : self::calculateLuminosity($color2);
    
        if ($l1 > $l2) {
            $ratio = (($l1 + 0.05) / ($l2 + 0.05));
        } else {
            $ratio = (($l2 + 0.05) / ($l1 + 0.05));
        }
        return $ratio;
    }

    /**
     * Returns an array with the results of the color contrast analysis.
     * 
     * It returns a key for each level (AA and AAA, both for normal and large text).
     * It also returns the calculated contrast ratio.
     * 
     * @see https://www.w3.org/TR/WCAG20/#visual-audio-contrast
     * @see https://www.w3.org/TR/WCAG20/#larger-scaledef
     * 
     * @param string|float $color1 First color in hexadecimal without hash or luminance as float
     * @param string|float $color2 Second color in hexadecimal without hash or luminance as float
     * 
     * @return array
     */
    public static function evaluateColorContrast($color1, $color2)
    {
        $ratio = self::calculateLuminosityRatio($color1, $color2);
    
        $colorEvaluation["levelAANormal"] = $ratio >= 4.5;
        $colorEvaluation["levelAALarge"] = $ratio >= 3;
        $colorEvaluation["levelAAANormal"] = $ratio >= 7;
        $colorEvaluation["levelAAALarge"] = $ratio >= 4.5;
        $colorEvaluation["ratio"] = $ratio;
    
        return $colorEvaluation;
    }

    /**
     * Get different lightnesses of an image.
     * 
     * @see https://stackoverflow.com/a/48851731 Origin of this function
     * 
     * @param FileReference $fileReference File reference
     * 
     * @return string Hexadecimal color in form of RRGGBB without hash (#)
     */
    public static function getImageLightness($fileReference)
    {
        $fileIdentifier = $fileReference->getHashedIdentifier();
        $luminance = [];

        if (! ($luminance = static::getLuminanceCache()->getValueForImage($fileIdentifier))) {
            $file = $fileReference->getForLocalProcessing(false);
            $img = new \Imagick($file);
            $img->cropImage($img->getImageWidth() - 24, $img->getImageHeight() - 24, 12, 12);

            $scaleWidth = $img->getImageWidth() >= 1000 ? 1000 : $img->getImageWidth();
            $scaleHeight = $img->getImageHeight() >= 1000 ? 1000 : $img->getImageHeight();
            if ($img->getImageWidth() > 1100 || $img->getImageHeight() > 1100) {
                $img->resizeImage($scaleWidth, $scaleHeight, \Imagick::FILTER_POINT, 1);
            }

            /** @var \ImagickPixel[] */
            $imgHistogramm = $img->getImageHistogram();
            $colors = [];

            foreach ($imgHistogramm as $pixel) {
                $rgbDec = $pixel->getColor();

                $rgb = str_pad(dechex($rgbDec['r']), 2, "0", STR_PAD_LEFT);
                $rgb .= str_pad(dechex($rgbDec['g']), 2, "0", STR_PAD_LEFT);
                $rgb .= str_pad(dechex($rgbDec['b']), 2, "0", STR_PAD_LEFT);

                $colors[] = ["rgb" => $rgb, "lum" => round(static::calculateLuminosity($rgb), 5)];
            }

            $colorLums = array_column($colors, 'lum');
            $colorMin = $colors[array_search(min($colorLums), $colorLums)];
            $colorMax = $colors[array_search(max($colorLums), $colorLums)];

            static::getLuminanceCache()->setValueForImage($fileIdentifier, $colorMin, $colorMax);
            $luminance = [
                'min' => $colorMin,
                'max' => $colorMax,
            ];
        }

        return $luminance;
    }

    private static function changeBrightness($color, $mode = 'lighten', $changeBy = 0.02)
    {
        $r = hexdec(substr($color, 0, 2)); // red value
        $g = hexdec(substr($color, 2, 2)); // green value
        $b = hexdec(substr($color, 4, 2)); // blue value
        $changeBy = ($mode == 'lighten' ? 1 + $changeBy : 1 - $changeBy);

        $r *= $changeBy;
        $g *= $changeBy;
        $b *= $changeBy;

        if ($r > 255) $r = 255;
        if ($g > 255) $g = 255;
        if ($b > 255) $b = 255;
        if ($r < 0) $r = 0;
        if ($g < 0) $g = 0;
        if ($b < 0) $b = 0;
        
        $return = str_pad(dechex($r), 2, "0", STR_PAD_LEFT);
        $return .= str_pad(dechex($g), 2, "0", STR_PAD_LEFT);
        $return .= str_pad(dechex($b), 2, "0", STR_PAD_LEFT);
        return $return;
    }

    public static function lightenColor($color, $changeBy = 0.02)
    {
        return static::changeBrightness($color, 'lighten', $changeBy);
    }

    public static function darkenColor($color, $changeBy = 0.02)
    {
        return static::changeBrightness($color, 'darken', $changeBy);
    }

    private static function getLuminanceCache()
    {
        if (!static::$luminanceCacheObj) {
            $om = GeneralUtility::makeInstance(CacheManager::class)->getCache('om_evergreen_img_luminance');
            static::$luminanceCacheObj = new ImgLuminance($om);
        }

        return static::$luminanceCacheObj;
    }
}

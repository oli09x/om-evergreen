<?php
namespace Olivermelle\OmEvergreen\StaticHelper;

class CleanStrings {
    /**
     * Clean hex color to get RRGGBB.
     * 
     * @see https://stackoverflow.com/a/67744236 Origin of this function
     * 
     * @param string $hex Hexadecimal color
     * 
     * @return string Hexadecimal color in form of RRGGBB without hash (#)
     */
    public static function cleanHexColor($hex)
    {
        $hex = strtolower($hex);

        //remove the leading "#"
        if (strlen($hex) == 7 || strlen($hex) == 4) {
            $hex = substr($hex, 1);
        }

        if (preg_match('/^[a-f0-9]{6}$/i', $hex)) {
            // $hex like "162a7b"
            return $hex;
        } elseif (preg_match('/^[a-f0-9]{3}$/i', $hex)) {
            // $hex like "1a7"
            return $hex[0] . $hex[0] . $hex[1] . $hex[1] . $hex[2] . $hex[2];
        } else {
            // invalid input
            throw new \UnexpectedValueException("Passed value is not a hexadecimal RGB value. Passed value: " . $hex);
        }
    }
}

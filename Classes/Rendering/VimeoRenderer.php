<?php
namespace Olivermelle\OmEvergreen\Rendering;

use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Localization\LanguageService;
use TYPO3\CMS\Core\Localization\LanguageServiceFactory;
use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Core\Resource\Rendering\VimeoRenderer as CoreRenderer;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class VimeoRenderer extends CoreRenderer
{
    /** @var ServerRequestInterface */
    private $request;

    /** @var string */
    private $baseUrl;

    /**@var LanguageService */
    private $languageService;

    public function __construct()
    {
        $languageServiceFactory = GeneralUtility::makeInstance(LanguageServiceFactory::class);

        $this->request = $GLOBALS['TYPO3_REQUEST'];
        $this->baseUrl = $this->request->getAttribute('site')->getBase();

        $lang = $this->request->getAttribute('language') ?? $this->request->getAttribute('site')->getDefaultLanguage();
        $this->languageService = $languageServiceFactory->createFromSiteLanguage($lang);
    }

    public function getPriority()
    {
        return 2;
    }

    public function render(FileInterface $file, $width, $height, array $options = [], $usedPathsRelativeToCurrentScript = false)
    {
        $id = $this->getVideoIdFromFile($file);
        $ratio = round($file->getProperty('height') / $file->getProperty('width'), 5) * 100;
        $classes = str_replace("img-thumbnail", "", $options["class"]);
        $consenttype = $this->str_starts_with($options['consenttype'], 'Usercentrics') ? 'data-usercentrics' : 'data-ome-consent';
        unset($options["class"]);

        $js = "if (typeof Vimeo == 'undefined') {var Vimeo = []};";
        $js .= "Vimeo.push([\"" . $id . "\", document.getElementById(\"" . $options["id"] . "\"), \"" . $this->baseUrl . "\"]);";
        $jsElem = "<script type=\"text/plain\" " . $consenttype . "=\"Vimeo\">" . $js . "</script>";

        $ratioInner = $this->languageService->sL('LLL:EXT:om_evergreen/Resources/Private/Language/locallang_fe.xlf:consentbanner.note');
        $ratioInner = sprintf($ratioInner, "Vimeo");
        $ratioElem = "<div id=\"" . $options["id"] . "\" class=\"ome-consent-note\">" . $ratioInner . "</div>";

        return "<div class=\"ratio " . $classes . "\" style=\"--bs-aspect-ratio: " . $ratio . "%\">" . $ratioElem . $jsElem . "</div>";
    }

    private function str_starts_with($haystack, $needle)
    {
        if (!function_exists('str_starts_with')) {
            return (string)$needle !== '' && strncmp($haystack, $needle, strlen($needle)) === 0;
        } else {
            return str_starts_with($haystack, $needle);
        }
    }
}

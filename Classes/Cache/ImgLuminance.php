<?php
namespace Olivermelle\OmEvergreen\Cache;

use TYPO3\CMS\Core\Cache\Frontend\FrontendInterface;

class ImgLuminance
{
    private $cache;

    public function __construct(FrontendInterface $cache)
    {
        $this->cache = $cache;
    }

    /**
     * Get the cached minimum and maximum luminance of an image
     * 
     * @param string $identifier Image identifier, mostly getHashedIdentifier() of a FileReference
     * 
     * @return array[] An array with the keys 'min' and 'max', each containing 'rgb' and 'lum'
     */
    public function getValueForImage($identifier)
    {
        return $this->cache->get($identifier);
    }

    /**
     * Set the cached minimum and maximum luminance of an image
     * 
     * @param string $identifier Image identifier, mostly getHashedIdentifier() of a FileReference
     * @param array $min Minimum luminance and rgb
     * @param array $max Maximum luminance and rgb
     * 
     * @return void
     */
    public function setValueForImage($identifier, $min, $max)
    {
        $this->cache->set($identifier, ['min' => $min, 'max' => $max]);
    }
}

/*!
  * Bootstrap v5.2.3 (https://getbootstrap.com/)
  * Copyright 2011-2023 The Bootstrap Authors (https://github.com/twbs/bootstrap/graphs/contributors)
  * Licensed under MIT (https://github.com/twbs/bootstrap/blob/main/LICENSE)
  */
class Youtube {
    /**
     * @param {string} id YouTube Video ID
     * @param {HTMLElement} elem HTML Element to be replaced with the YouTube iframe
     * @param {string} origin Origin URL (for analytic purposes)
     */
    constructor (id, elem, origin) {
        this.addScriptTag();
        elem.classList.remove('ome-consent-note');

        if (typeof window.onYouTubeIframeAPIReady !== 'function') {
            window.onYouTubeIframeAPIReady = function() {
                for (const elem of Youtube.onYTLoadedArr) {
                    new YT.Player(elem[1], {
                        videoId: elem[0],
                        playerVars: {
                            enablejsapi: 1,
                            rel: 0,
                            showinfo: 0,
                            origin: elem[2]
                        }
                    });
                }
            };
        }

        if (typeof YT === 'undefined') {
            Youtube.onYTLoadedArr.push([id, elem, origin]);
        } else {
            new YT.Player(elem, {
                videoId: id,
                playerVars: {
                    enablejsapi: 1,
                    rel: 0,
                    showinfo: 0,
                    origin: origin
                }
            });
        }
    }

    addScriptTag() {
        if (!document.querySelector("#youtube-script-elem")) {
            const elem = document.createElement('script');
            elem.id = "youtube-script-elem";
            elem.src = "https://www.youtube.com/iframe_api";
            document.head.appendChild(elem);
        }
    }

    static onYTLoadedArr = [];

    static push(item) {
        new Youtube(item[0], item[1], item[2]);
    }

    /**
     * Handle already registered actions
     * @param {Array} array History of calls
     */
    static _processHistory(array) {
        array.forEach(item => new Youtube(item[0], item[1], item[2]));
    }
}

export { Youtube as default };
//# sourceMappingURL=youtube-b810c3ff.js.map

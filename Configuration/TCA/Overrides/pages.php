<?php

use TYPO3\CMS\Core\Information\Typo3Version;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

defined('TYPO3') or die();

(function() {
    $versionObj = GeneralUtility::makeInstance(Typo3Version::class);

    if ($versionObj->getMajorVersion() < 12) {
        $GLOBALS['TCA']['pages']['columns']['media']['config']['overrideChildTca']['columns']['uid_local']['config']['appearance']['elementBrowserAllowed'] =
            $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'];
        
        $faviconTCAConfig = ExtensionManagementUtility::getFileFieldTCAConfig(
            'tx_om_evergreen_favicon',
            [
                'maxitems' => 1,
                'appearance' => [
                    'fileByUrlAllowed' => false,
                ]
            ],
            'ico'
        );
    } else {
        $GLOBALS['TCA']['pages']['columns']['media']['config']['allowed'] = 'common-image-types';

        $faviconTCAConfig = [
            'type' => 'file',
            'maxitems' => 1,
            'allowed' => 'ico',
            'appearance' => [
                'fileByUrlAllowed' => false
            ]
        ];
    };

    $GLOBALS['TCA']['pages']['columns']['media']['config']['maxitems'] = 1;
    $GLOBALS['TCA']['pages']['columns']['media']['config']['appearance'] = [
        'fileByUrlAllowed' => false
    ];

    $faviconTCA = [
        'tx_om_evergreen_favicon' => [
            'label' => 'LLL:EXT:om_evergreen/Resources/Private/Language/locallang.xlf:pages.favicon',
            'description' => 'LLL:EXT:om_evergreen/Resources/Private/Language/locallang.xlf:pages.favicon.description',
            'l10n_mode' => 'exclude',
            'config' => $faviconTCAConfig
        ]
    ];
    ExtensionManagementUtility::addTCAcolumns('pages', $faviconTCA);
    ExtensionManagementUtility::addFieldsToPalette('pages', 'media', 'tx_om_evergreen_favicon', 'after:media');
})();

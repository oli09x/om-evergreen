<?php

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

defined('TYPO3') or die();

(function() {
    ExtensionManagementUtility::addTcaSelectItem(
        'tt_content',
        'CType',
        [
            'LLL:EXT:om_evergreen/Resources/Private/Language/locallang.xlf:ttContent.ceheadertextimg.title',
            'om_evergreen_headertextimg',
            'CEHeaderTextImg',
        ],
        'textmedia',
        'after'
    );
    $GLOBALS['TCA']['tt_content']['types']['om_evergreen_headertextimg'] = [
        'showitem' => '
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
                --palette--;;general,
                --palette--;;headers,
                bodytext;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:bodytext_formlabel,
            --div--;LLL:EXT:om_evergreen/Resources/Private/Language/locallang.xlf:ttContent.tab.backgroundImage,
                image,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
                --palette--;;language,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
                --palette--;;hidden,
                --palette--;;access,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,
                rowDescription,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,
        ',
        'columnsOverrides' => [
            'bodytext' => [
                'config' => [
                    'enableRichtext' => true,
                    'richtextConfiguration' => 'default',
                ],
            ],
            'image' => [
                'config' => [
                    'maxitems' => 1,
                    'overrideChildTca' => [
                        'types' => [
                            2 => [
                                'showitem' => 'crop, --palette--;;filePalette',
                            ]
                        ]
                    ]
                ]
            ],
            'sectionIndex' => [
                'config' => [
                    'default' => 0
                ]
            ]
        ],
    ];
})();

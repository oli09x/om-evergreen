<?php

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

defined('TYPO3') or die();

/** Make TypoScript available for template inclusion */
ExtensionManagementUtility::addStaticFile(
    'om_evergreen',
    'Configuration/TypoScript',
    'TypoScript needed for OM Evergreen'
);

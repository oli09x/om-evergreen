<?php

use Olivermelle\OmEvergreen\Rendering\VimeoRenderer;
use Olivermelle\OmEvergreen\Rendering\YouTubeRenderer;
use TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider;
use TYPO3\CMS\Core\Imaging\IconRegistry;
use TYPO3\CMS\Core\Information\Typo3Version;
use TYPO3\CMS\Core\Resource\Rendering\RendererRegistry;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

defined('TYPO3') or die();

(function() {
    $versionObj = GeneralUtility::makeInstance(Typo3Version::class);
    $iconRegistry = GeneralUtility::makeInstance(IconRegistry::class);

    /** Import page tsconfig */
    if ($versionObj->getMajorVersion() < 12) {
        ExtensionManagementUtility::addPageTSConfig('@import "EXT:om_evergreen/Configuration/page.tsconfig"');
    }

    $iconRegistry->registerIcon('CEHeaderTextImg', SvgIconProvider::class, ['source' => 'EXT:om_evergreen/Resources/Public/Icons/CEHeaderTextImg.svg']);

    $GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['ome_default'] = 'EXT:om_evergreen/Configuration/RTE/OMEDefault.yaml';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['om_evergreen_img_luminance'] ??= [];
    
    $rendererRegistry = GeneralUtility::makeInstance(RendererRegistry::class);
    $rendererRegistry->registerRendererClass(YouTubeRenderer::class);
    $rendererRegistry->registerRendererClass(VimeoRenderer::class);

    foreach (['tx_om_evergreen_favicon'] as $field) {
        if (!in_array($field, explode(',', $GLOBALS["TYPO3_CONF_VARS"]["FE"]["addRootLineFields"]))) {
            $addComma = empty($GLOBALS["TYPO3_CONF_VARS"]["FE"]["addRootLineFields"]) ? '' : ',';
            $GLOBALS["TYPO3_CONF_VARS"]["FE"]["addRootLineFields"] .= $addComma.$field;
        }
    }

    foreach (['ico'] as $ext) {
        if (!in_array($ext, explode(',', $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']))) {
            $addComma = empty($GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']) ? '' : ',';
            $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'] .= $addComma.$ext;
        }
    }
})();

<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'OM Evergreen',
    'description' => 'Extendable, multipurpose Typo3 template. Responsive, SEO friendly and GDPR compliant. Available May 2023.',
    'version' => '1.0.1',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-11.5.99',
            'fluid_styled_content' => '11.5.0-11.5.99',
            'seo' => '11.5.0-11.5.99',
            'rte_ckeditor' => '11.5.0-11.5.99',
            'vhs' => '6.1.3-6.1.99',
            'content-defender' => '3.3.0-3.3.99',
        ],
    ],
    'state' => 'stable',
    'author' => 'Oliver Melle',
    'author_email' => 'dev@oliver-melle.com',
    'autoload' => [
        'psr-4' => [
            'Olivermelle\\OmEvergreen\\' => 'Classes',
        ],
    ],
];
